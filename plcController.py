#PLC Controlling with Python Ahmet CAN
import snap7
import snap7.client as plc
from snap7.util import *
from time import sleep
from snap7.snap7types import *

# 0x81 = areas['PE'] Inputs
# 0x82 = areas['PA'] Outputs
# 0x83 = areas['MK'] Memory


def WriteOutput(plc,bytebit,cmd):
    byte,bit = bytebit.split('.')
    byte,bit = int(byte),int(bit)
    data = plc.read_area(areas['PA'],0,byte,1)
    set_bool(data,byte,bit,cmd)
    plc.write_area(areas['PA'],0,byte,data)
    
def ReadOutput(plc,bytebit):
    byte,bit = bytebit.split('.')
    byte,bit = int(byte),int(bit)
    data = plc.read_area(areas['PA'],0,byte,1)
    status = get_bool(data,byte,bit)
    return status

def ReadMemory(plc,byte,datatype):
    result = plc.read_area(areas['MK'],0,byte,datatype)
    if datatype == S7WLBit:
        return get_bool(result,0,1)
    if(datatype == S7WLByte or datatype == S7WLWord):
        return get_int(result,0)
    if(datatype == S7WLReal):
        return get_real(result,0)
    if(datatype == S7WLDWord):
        return get_dword(result,0)
    else:
        return None

def WriteMemory(plc,byte,bit,datatype,value):
    result = plc.read_area(areas['MK'],0,byte,datatype)
    if datatype == S7WLBit:
        set_bool(result,0,bit,value)
    elif datatype == S7WLByte or datatype == S7WLWord:
        set_int(result,0,value)
    elif datatype == S7WLReal:
        set_real(result,0,value)
    elif datatype == S7WDWord:
        set_dword(result,0,value)

    plc.write_area(areas['MK'],0,byte,result)
    print('Veri aktarildi')
    

myplc = snap7.client.Client()
myplc.connect('10.10.54.2',0,1)
print(myplc.get_connected())

print WriteMemory(myplc,100,0,S7WLReal,100)
print ReadMemory(myplc,100,S7WLReal)
WriteOutput(myplc,'0.0',0)

#count = 0
#for x in range(100):
#    count = count + 1
#    if count%2 == 0:
#        WriteOutput(myplc,'0.0',1)
#        print("Q0.0 : Aktif")
#    else:
#        WriteOutput(myplc,'0.0',0)
#        print("Q0.0 : Pasif")
#    sleep(1)
